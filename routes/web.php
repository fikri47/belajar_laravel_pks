<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\KritikController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', [IndexController::class, 'home']);

Route::get('/register', [AuthController::class, 'register']);

Route::post('/welcome',[AuthController::class, 'welcome']);

Route::get('/data-tables', [IndexController::class, 'dataTable']);


Route::resource('cast', CastController::class);
Route::resource('profile', ProfileController::class);
Route::resource('genre', GenreController::class);
Route::resource('film', FilmController::class);

Route::post('/kritik/{film_id}', [KritikController::class, 'tambah']);



Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
