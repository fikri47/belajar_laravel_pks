@extends('layout.master')

@section('title', 'Tambah Data Cast')
@section('content')

<h2>Tambah Data</h2>
<form action="/cast" method="POST">
    @csrf
    <div class="row g-3">
        <div class="col">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" min="0"class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="bio" class="form-label">Masukan Bio</label>
        <textarea class="form-control" name="bio" id="bio" rows="3" placeholder="Masukan Bio"></textarea>      
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection