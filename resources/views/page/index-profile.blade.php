@extends('layout.master')

@section('title', 'Edit Profile')

@section('content')
<h2>Edit Data</h2>
<form action="/profile/{{$detailProfile->id}}" method="POST">
    @csrf
    @method('PUT')
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" class="form-control" name="name" value="{{$detailProfile->user->name}}"id="name" disabled>                
            </div>
            <div class="form-group">
                <label for="email">Email User</label>
                <input type="text" class="form-control" name="email" value="{{$detailProfile->user->email}}"id="email" disabled>                
            </div>                
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" min="0"class="form-control" name="umur" value="{{$detailProfile->umur}}" id="umur" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>            
    <div class="form-group">
        <label for="bio" class="form-label">Masukan Bio</label>
        <textarea class="form-control" name="bio" value="" id="bio" rows="3" placeholder="Masukan Bio">{{$detailProfile->bio}}</textarea>      
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="alamat" class="form-label">Masukan Alamat</label>
        <textarea class="form-control" name="alamat" value="" id="alamat" rows="3" placeholder="Masukan Alamat">{{$detailProfile->alamat}}</textarea>      
        @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>
</form>
@endsection