@extends('layout.master')

@section('title', 'Data Cast')

@section('content')
<a href="/cast/create" class="btn btn-primary mb-3">Tambah</a>
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$value)
                    <tr >
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td>                            
                            <div class="d-flex ">
                            <a href="/cast/{{$value->id}}" class="btn btn-info mr-2" >Show</a>
                            @auth
                                
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-primary mr-2">Edit</a>
                                <form action="/cast/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger" onlclick="showAlert()"value="Delete">
                                </form>                            
                            @endauth
                            </div>
                        </td>
                        
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
    </div>
@endsection