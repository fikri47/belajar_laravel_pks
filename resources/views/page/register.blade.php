@extends('layout.master')

@section('title', 'Halaman Form')

@section('content')
<h2>Buat Account Baru</h2>
<h3>Sign Up Form</h3>
<form action="/welcome" method="POST">
@csrf
  <label for="name">First Name:</label><br> <br>
  <input type="text" name="firstName" id="" /><br> <br>
  <label for="">Last Name:</label><br> <br>
  <input type="text" name="lastName" id="" /><br> <br>
  <label for="">Gender</label><br>
  <input type="radio" name="gender" id="" />Male<br>
  <input type="radio" name="gender" id="" />Female<br> <br>
  <label for="nationality">Nationality</label><br> <br>
  <select name="nationality" id="">
    <option value="">Indonesian</option>
    <option value="">American</option>
    <option value="">English</option>
  </select><br> <br>
  <label for="language">Language Spoken</label><br> <br>
  <input type="checkbox" name="language" />Bahasa<br>
  <input type="checkbox" name="language" />English<br>
  <input type="checkbox" name="language" />Russian<br> <br>
  <label for="bio">bio</label><br><br>
  <textarea
    name="pesan"
    id="pesan"
    cols="30"
    rows="10"
    placeholder="Silahkan Masukan Biodata Anda"
  ></textarea><br>
  <input type="submit" value="Sign up">
</form>
@endsection