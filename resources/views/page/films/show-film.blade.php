@extends('layout.master')

@section('title')
<h4>Film {{$film->judul}}</h4>
@endsection

@section('content')

<p class="card-text">{{$film->ringkasan}}</p>


<hr>
<h4>List Kritik</h4>
<div class="card-footer card-comments pl-2 mt-4">
    <div class="card-comment mb-3">
    @forelse ($film->kritik as $item)    
    <img class="img-circle img-sm" src={{asset("admin/dist/img/user2-160x160.jpg")}} alt="User Image">

    <div class="comment-text">
      <span class="username">
        {{$item->user->name}}
        <span class="text-muted float-right">{{$item->user->created_at}}</span>
      </span><!-- /.username -->
      {{$item->content}}
    </div>
    <!-- /.comment-text -->
  </div>
    @empty
        <p>No Comment</p>
        <!-- /.card-comment -->
    </div>
    @endforelse             
<hr>
<form action="/kritik/{{$film->id}}" method="post">
    @csrf
    <textarea name="content" class="form-control" id="content" cols="30" rows="3" placeholder="isi content"></textarea>
        @error('content')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror    
    <input type="submit" class="btn btn-outline-dark mt-2" value="Kritik">    
</form>


@endsection

