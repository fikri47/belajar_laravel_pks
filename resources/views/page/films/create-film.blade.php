@extends('layout.master')

@section('title', 'Tambah Data Film')
@section('content')

<h2>Tambah Data</h2>
<form action="/film" method="POST">
    @csrf
    <div class="row g-3">
        <div class="col">
            <div class="form-group">
                <label for="judul">judul</label>
                <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="tahun">Tahun</label>
                <input type="number" min="0"class="form-control" name="tahun" id="tahun" placeholder="Masukkan tahun">
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="poster">poster</label>
        <input type="text" class="form-control" name="poster" id="poster" placeholder="Masukkan poster">
        @error('tahun')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror        
    </div>
    <div class="form-group">
        <label for="ringkasan" class="form-label">Masukan ringkasan</label>
        <textarea class="form-control" name="ringkasan" id="ringkasan" rows="3" placeholder="Masukan ringkasan"></textarea>      
        @error('ringkasan')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>    
    <div class="form-group">
        <label>Genre</label>
        <select class="form-control" name="genre">
          @foreach ($genre as $value)
          <option value="{{$value->id}}">{{$value->nama}}</option>              
          @endforeach
        </select>
      </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection