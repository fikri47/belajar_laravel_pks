@extends('layout.master')

@section('title', 'Data Film')

@section('content')
<a href="/film/create" class="btn btn-primary mb-3">Tambah</a>
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Judul</th>
                <th scope="col">Ringkasan</th>                
                <th scope="col">Tahun</th>                
                <th scope="col">Genre</th>
                <th scope="col">Poster</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($film as $key=>$value)                
                    <tr >
                        <td>{{$key + 1}}</th>
                        <td>{{$value->judul}}</td>
                        <td>{{$value->ringkasan}}</td>
                        <td>{{$value->tahun}}</td>
                        <td>{{$value->genre ? $value->genre->nama : "-"}}</td>
                        <td>{{$value->poster}}</td>                        
                        <td>                            
                            <div class="d-flex ">
                            <a href="/film/{{$value->id}}" class="btn btn-info mr-2" >Show</a>
                            @auth
                                
                            <a href="/film/{{$value->id}}/edit" class="btn btn-primary mr-2">Edit</a>
                                <form action="/film/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger" onlclick="showAlert()"value="Delete">
                                </form>                            
                            @endauth
                            </div>
                        </td>
                        
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
    </div>
@endsection