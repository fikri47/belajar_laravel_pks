@extends('layout.master')

@section('title', 'Data Genre')

@section('content')
<a href="/genre/create" class="btn btn-primary mb-3">Tambah</a>
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Nama Genre</th>                
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($genre as $key=>$value)
                    <tr >
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>                            
                            <div class="d-flex ">
                            <a href="/genre/{{$value->id}}" class="btn btn-info mr-2" >Show</a>
                            @auth
                                
                            <a href="/genre/{{$value->id}}/edit" class="btn btn-primary mr-2">Edit</a>
                                <form action="/genre/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger" onlclick="showAlert()"value="Delete">
                                </form>                            
                            @endauth
                            </div>
                        </td>
                        
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
    </div>
@endsection