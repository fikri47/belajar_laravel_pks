@extends('layout.master')

@section('title', 'Tambah Data Genre')
@section('content')

<h2>Tambah Data</h2>
<form action="/genre" method="POST">    
    @csrf
           <div class="form-group">
                <label for="nama">Genre</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Genre">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>        
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection