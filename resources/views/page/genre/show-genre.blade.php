@extends('layout.master')

@section('title')
<h4>Genre {{$genre->nama}}</h4>
@endsection

@section('content')
{{-- {{dd($genre->film)}} --}}
<div class="table-responsive">
    <table class="table">
        <thead class="thead-light">
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Judul</th>
            <th scope="col">Ringkasan</th>                
            <th scope="col">Tahun</th>                
            <th scope="col">Genre</th>
            <th scope="col">Poster</th>            
          </tr>
        </thead>
        <tbody>
            @forelse ($genre->film as $key=>$value)                
                <tr >
                    <td>{{$key + 1}}</th>
                    <td>{{$value->judul}}</td>
                    <td>{{$value->ringkasan}}</td>
                    <td>{{$value->tahun}}</td>
                    <td>{{$value->genre ? $value->genre->nama : "-"}}</td>
                    <td>{{$value->poster}}</td>                                                                
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>
</div>

@endsection