@extends('layout.master')

@section('content')
<div class="card" style="width: 18rem;">
    <div class="card-body">
      <h5 class="card-title">{{$cast->nama}}</h5>
      <h6 class="card-subtitle mt-4 text-muted">Umur : {{$cast->umur}}</h6>
      <p class="card-text">{{$cast->bio}}</p>      
    </div>
  </div>
@endsection