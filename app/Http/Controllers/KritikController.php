<?php

namespace App\Http\Controllers;
use App\Models\Kritik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KritikController extends Controller
{
    public function tambah(Request $request, $id) {
        if (!Auth::check()) {
            return redirect('/login');  
        }

        $request->validate([
            'content'=> 'required'
        ]);

        $iduser = Auth::id();
        
        $kritik = new Kritik();
        $kritik->user_id = $iduser;
        $kritik->content = $request -> content;
        $kritik->film_id = $id;

        $kritik->save();

        return redirect('/film/'.$id);

    }
}
