<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->only(['index','update']);
    }
    public function index() {
        $idUser = Auth::id();

        $detailProfile = Profile::where("user_id", $idUser)->first();

        return view('page.index-profile', ['detailProfile' => $detailProfile]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([            
            'umur'=> 'required|numeric',
            'bio'=> 'required',
            'alamat'=> 'required',               
        ]);

        $profile = Profile::find($id);        
        $profile -> umur = $request->umur;
        $profile -> bio = $request->bio;
        $profile -> alamat = $request->alamat;
        $profile->update();

        return redirect('/');
    }
}
