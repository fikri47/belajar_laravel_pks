<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Genre;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct() {
        $this->middleware('auth')->except(['index','show']);
    }

    public function index()
    {
        $film = Film::all();
        return view('page.films.index-film', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $genre = Genre::get();        
        return view('page.films.create-film', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $this->validate($request, [
            'judul' => 'required',
            'ringkasan'=> 'required',            
            'tahun'=> 'required|numeric',
            'poster'=> 'required',            
            'genre'=> 'required'
        ]);

        // Film::create([
        //     'judul' => $request->judul,
        //     'ringkasan'=> $request->ringkasan,   
        //     'tahun'=> $request->tahun,
        //     'poster'=> $request->poster,
        //     'genre'=>$request->genre
        // ]);
        
        $film = new Film();    
        $film -> judul = $request->judul;
        $film -> ringkasan = $request->ringkasan;
        $film -> tahun = $request->tahun;
        $film -> poster = $request->poster;        
        $film -> genre_id = $request->genre;
        $film->save();

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);
        return view('page.films.show-film', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
