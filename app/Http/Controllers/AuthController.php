<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register () {
        return view('page.register');
    }

    public function welcome(Request $request) {
        $first_name = $request['firstName'];
        $last_name = $request['lastName'];

        return view('page.welcome', ['firstName' => $first_name, 'lastName' => $last_name]);
    }
}
